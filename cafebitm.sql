-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2016 at 10:40 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cafebitm`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `menuItems` varchar(255) NOT NULL,
  `price` int(10) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `category`, `menuItems`, `price`, `image`) VALUES
(25, 'Dinner', 'Salad', 211, ''),
(26, 'Dinner', 'Salad', 211, ''),
(27, 'Dinner', 'Prawn', 211, ''),
(29, 'dafewaf', 'afeawfwaf', 100, ''),
(30, 'asf', '', 0, ''),
(31, 'afeafe', 'afweafe', 12121, ''),
(32, 'afewafe', 'afewfe', 12121, '1-2-2.JPG'),
(33, 'afewa', 'fafew', 0, ''),
(34, 'Dinner', 'eafewf', 10, ''),
(35, 'Dinner', 'eafewf', 10, ''),
(36, 'afeawf', 'eafewf', 12121, 'http://localhost/CafeBitm/views/uploads/uploads/avatar3.jpg'),
(37, 'afeawf', 'eafewf', 12121, 'http://localhost/CafeBitm/views/uploads/uploads/avatar3.jpg'),
(38, 'Dinner', 'Salad', 10, 'http://localhost/CafeBitm/views/uploads/twitter.png'),
(39, '', '', 0, 'http://localhost/CafeBitm/views/uploads/avatar3.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
