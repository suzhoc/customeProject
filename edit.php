<?php 

include_once('header.php'); 
include_once('src/cafe/MenuItems.php'); 

use \CafeBITM\cafe\MenuItems; 

$obj = new MenuItems(); 
$menus = $obj->show($_GET); 
?>




<div style = "margin: auto; width: 80%">
<form  class="form-horizontal" role="form" action = "update.php" method = "post" enctype="multipart/form-data">
    <input type ="hidden" name ="id" value ="<?php echo $menus['id']?>">
  <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
           <img alt = "Currently there is No Image" src ="<?php echo $menus['image']?>" width = "150px">
          
          
      </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2"  for="email">Student Name</label>
    <div class="col-sm-10">
    <input type ="text" name ="name" value = "<?php echo $menus['name']; ?>" class="form-control" id="email" >
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Course Taken:</label>
    <div class="col-sm-10">
        
    <select class="form-control" name = "course">
            <option value="Java">Java</option>
            <option value="Python">Python</option>
            <option value="PHP">PHP</option>
            <option value="JavaScript">JavaScript</option>
             <option value="Visual Basic .NET">Visual Basic .NET</option>
             <option value="Objective-C">Objective-C</option>
              <option value="Perl">Perl</option>
    </select>     
        
    </div>
  </div>
 <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Change Image:</label>
    <div class="col-sm-10">
    <input type ="file" name ="image">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
  <button type="submit" class="btn btn-default">Submit</button>
    </div>
  </div>
</form>



 