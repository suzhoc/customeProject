<?php
ini_set("display_errors","On");
error_reporting(E_ALL & ~E_DEPRECATED);
include_once('src/cafe/MenuItems.php'); 
include_once('vendor/mpdf/mpdf/mpdf.php');
use \CafeBITM\cafe\MenuItems; 


$obj = new MenuItems(); 
$std = $obj->index();
$trs = "";

?>


                
                <?php
                $slno =0;
                foreach($std as $menus):
                    $slno++;
                    $trs .="<tr>";
                    $trs .="<td>".$slno."</td>";
                    $trs .="<td>".$menus['name']."</td>";
                    $trs .="<td>".$menus['course']."</td>";
                    $trs .="<td><img src=".$menus['image']." height='50' width='50' /></td>";
                    $trs .="</tr>";
                 endforeach;   
                ?>


<?php

$html = <<<BITM
<!DOCTYPE html>
<html>
    <head>
        <title>List of Books</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .delete{
                border:1px solid red;
            }
        </style>
    
    </head>
    
    <body>
        <h1>Student List</h1>
     
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    
                    <th>Student Name </th>
                    
                    <th>Course</th>
                    <th>Picture</th>
                     
                
                </tr>
            </thead>
            <tbody>
        
              echo $trs;
        
        
        
        </tbody>
        </table>
       
        
            

    </body>
</html>
BITM;
?>
<?php

require_once $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."cafeBITM".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf.php';

$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

