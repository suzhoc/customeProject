<?php
namespace CafeBITM\cafe; 
include_once('Model.php'); 
include_once('Utility.php'); 

use CafeBITM\cafe\Model; 
use CafeBITM\Utility\Utility; 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of menuItems
 *
 * @author personal
 */
class MenuItems extends Model{
    public $table = 'menu'; 
    public $id = ""; 
    public $itemsName = ""; 
    public $itemCategory = ""; 
    
    public function __construct() {
        parent::__construct();
    }
    
    public function store($data  = false) {
        $data = array($this->table=>$data)  ;
                
        if($this->insert($data)){
            Utility::message("Student Info added successfully.");           
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
       Utility::redirect('index.php');
        
        
    }
   
    public function update($data = false) {
        var_dump($data); 
        $this->name = $data['name']; 
        $this->course = $data['course']; 
        $this->id = $data['id']; 
        if($this->image) {
        $query = "UPDATE `$this->table` SET `name` = '$this->name', `course` = '$this->course', `image` = '$this->image' WHERE `menu`.`id` = '$this->id';" ;
        } 
        else {
        $query = "UPDATE `$this->table` SET `name` = '$this->name', `course` = '$this->course' WHERE `menu`.`id` = '$this->id';" ;
        }
        $result = mysql_query($query); 
        
        if($result) {
            Utility::message("<span style = 'color: green;'>Student Info Updated successfully.</span>");           
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
       Utility::redirect('index.php');
    }
    
    
    public function index( $data=array()) {
       $whereClause = "AND 1=1";
       if(array_key_exists('search',$data)){
            $whereClause .= " AND name LIKE '%".$data['search']."%'";
            $whereClause .= " OR deleted_at IS NULL AND course LIKE '%".$data['search']."%'";
        }else{
       $filter = $data;
            if(is_array($filter) && count($filter) > 0){

              if(array_key_exists('filterName', $filter) && !empty($filter['filterName']) ){
                  $whereClause .= " AND name LIKE '%".$filter['filterName']."%'";
              }

              if(array_key_exists('filterCourse', $filter)  && !empty($filter['filterCourse']) ){
                  $whereClause .= " AND course LIKE '%".$filter['filterCourse']."%'";
              }

            }
        } 
        
        $datalist = array();
        
        $query = "SELECT * FROM `$this->table` WHERE deleted_at IS NULL ".$whereClause;
        $result = mysql_query($query);
        while($row = mysql_fetch_assoc($result)){
            $datalist[] = $row;
        }
        
        return $datalist;
        
    }
    
    public function trashed( $data=array()) {
        $whereClause = "AND 1=1";
       $filter = $data;
            if(is_array($filter) && count($filter) > 0){

              if(array_key_exists('filterName', $filter) && !empty($filter['filterName']) ){
                  $whereClause .= " AND name LIKE '%".$filter['filterName']."%'";
              }

              if(array_key_exists('filterCourse', $filter)  && !empty($filter['filterCourse']) ){
                  $whereClause .= " AND course LIKE '%".$filter['filterCourse']."%'";
              }

            }
       
        
        $datalist = array();
        
        $query = "SELECT * FROM `$this->table` WHERE deleted_at IS NOT NULL ".$whereClause;
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $datalist[] = $row;
        }
        
        return $datalist;
        
    }
    
    
    public function show($data = false) {
        $data = array($this->table=>$data); 
        $menus = $this->select($data); 
        return $menus;   
        
    }
    
    
    public function trash($data) {
        $this->id = $data['id']; 
        $this->deleted_at = time();
        $query = "UPDATE `$this->table` SET `deleted_at` = '$this->deleted_at' WHERE `menu`.`id` = '$this->id';" ;
        $result = mysql_query($query); 
        
        if($result) {
            Utility::message("<span style = 'color: red;'>Student Info Deleted successfully.</span>");           
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
       Utility::redirect('index.php');
    }
    
    public function recover($data) {
        $this->id = $data['id']; 
        $query = "UPDATE `$this->table` SET `deleted_at` = NULL WHERE `menu`.`id` = '$this->id';" ;        
        $result = mysql_query($query); 
        
        if($result) {
            Utility::message("<span style = 'color: green;'>Student Info Recovered successfully.</span>");           
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
       Utility::redirect('index.php');
        
    }
    
    public function delete($data) {
        $this->id = $data['id']; 
        $query = "DELETE FROM `$this->table` WHERE `menu`.`id` = '$this->id';" ;
        $result = mysql_query($query); 
        
        if($result) {
            Utility::message("<span style = 'color: red;'>Student Info Deleted successfully.</span>");           
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
       Utility::redirect('trashed.php');

    }
    
}
