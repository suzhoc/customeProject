<?php
namespace CafeBITM\cafe;
include_once('connectdb.php'); 

use CafeBITM\conn\connector; 
/**
 * Cafe BITM
 *
 * @Masum BITM 107477
 */
abstract class Model {
    public $table = ""; 
    
    public function __construct() {
        connector::connect(); 
    }
    
    public function insert($data = array()) {
        $_arrColumns = array_keys($data[$this->table]);
        $_strColumns = "`".implode("`, `", $_arrColumns)."`"; 
        
        $_arrValues = array_values($data[$this->table]); 
        $_strValues = "'".implode("', '", $_arrValues)."'"; 
        
        $query = "INSERT INTO $this->table ($_strColumns) VALUES ($_strValues)"; 
        $result = mysql_query($query); 
        if($result) {
            $data[$this->table]['id'] = mysql_insert_id();
            return $data;
        }
        
    }
    
    
    public function trashedAll() {
        $query = "SELECT * FROM `$this->table` WHERE deleted_at IS NOT NULL" ; 
   
        $result = mysql_query($query); 
        
        while($row = mysql_fetch_assoc($result)){
            $datalist[] = $row;
            }
            return $datalist; 
    }
    
    public function select($data)  {
        
        $this->id = $data[$this->table]['id']; 
        $query = "SELECT * FROM `$this->table` WHERE id = '$this->id'"; 
        $result = mysql_query($query); 
        $result = mysql_fetch_assoc($result); 
        return $result;
        
    }
    
    public function delete($data) {
        
        $id = $data[$this->table]['id']; 
        
        $_arrColumns = array_keys($data[$this->table]);
        $_strColumns = "`".implode("`, `", $_arrColumns)."`"; 
        
        $_arrValues = array_values($data[$this->table]); 
        $_strValues = "'".implode("', '", $_arrValues)."'"; 
            
        $query = "DELETE '$this->table' WHERE '$_strColumns' = '$_strValues'"; 
        $result = mysql_query($query); 
        $result = mysql_fetch_assoc($result); 
        return $result;
    }
    
    
    public function update($data) {
        
    }
}
