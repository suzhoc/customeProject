<?php


if(isset($_FILES['image'])) {
    $file = $_FILES['image']; 
    


    $file_name = $file['name']; 
    $file_tmp = $file['tmp_name'];
    $file_size = $file['size'];
    $file_error = $file['error']; 
    
    $file_ext = explode('.', $file_name); 
    $file_ext = strtolower(end($file_ext)); 
    
    $allowed = array('jpg', 'png'); 
    
    if(in_array($file_ext, $allowed)) {
        if($file_error === 0) {
            $file_destination = 'uploads/'.$file_name; 
            
            if(move_uploaded_file($file_tmp, $file_destination)) {
                echo $file_destination; 
            }
        }
    }
   
   
}   
?> 