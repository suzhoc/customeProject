<?php 

include_once('header.php'); 
include_once('src/cafe/MenuItems.php'); 
include_once('utility.php');


use \CafeBITM\cafe\MenuItems; 
use CafeBITM\Utility\Utility; 


$obj = new MenuItems(); 

    $filter=array();
    $filterName = isset($_POST['filterName'])?$_POST['filterName']:"";
    $filterCourse  = isset($_POST['filterCourse'])?$_POST['filterCourse']:"";
    

    if( strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
          $filter = $_POST;
          $menus = $obj->trashed($filter);
    }
    
    if( strtoupper($_SERVER['REQUEST_METHOD']) == 'GET'){
          $menus = $obj->trashed(); 
          
    }

?>


<div style = "margin: auto; width: 80%">
    <div class="row">
        <form action="trashed.php" method="post" >
            <div>
                <label>Filter By Name:</label>
                <input type="text" name="filterName" value="<?php echo $filterName;?>" /> 
                
                <label>Filter By Course:</label>
                <input type="text" name="filterCourse" id="filterTitle" value="<?php echo $filterCourse;?>" /> 
                
                <button type="submit"> GO </button>
            </div>
        </form>

        <br>
                        <div class="<?php $message = Utility::message(); if(!($message === "")) { echo 'alert alert-success'; }?>">
            <?php 
            echo $message; 
            ?>            
        </div>
        <?php if(count($menus) > 0){ foreach($menus as $menu) { ?>
        <div class="col-xs-6 col-md-3">
            <p style = "text-align: center;">
              <img src ="<?php echo $menu['image']; ?>" style = "width: 100px; height: 100px;" >
            </p>
            <h3 style = "text-align: center;"><?php echo $menu['name']; ?></h3>
            <h4 style = "text-align: center;"><?php echo "Course: ".$menu['course']; ?></h4> 
            <p style = "text-align: center;">
                <a href = "delete.php?id=<?php echo $menu['id']?>" class="trash btn btn-default btn-sm">
                <span class="glyphicon glyphicon-remove"></span> Delete
                </a>
       
        
                <a href = "recover.php?id=<?php echo $menu['id']?>" class = "btn btn-default btn-sm" >
                    <span class="glyphicon glyphicon-edit glyphicon"></span> Recover
                </a>
            </p>
            <hr>
        </div>
        <?php }} else{
                   ?>
                <tr><td colspan="5"> No record found</td></tr>
                <?php
               }?>
    </div>
    
    
</div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
           $('.trash').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 

</script>

<?php include_once('footer.php'); ?>